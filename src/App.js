import React, {Component} from 'react';
import {Redirect, BrowserRouter, Route, Switch} from 'react-router-dom';
import LoginForm from './components/Connexion';
import Candidats from './components/Candidats';
import './App.css';
import 'antd/dist/antd.css';

export default class App extends Component {
  
  render() {

    return(
      <BrowserRouter basename={`${process.env.PUBLIC_URL}/`}>
        <Switch>
          <Route exact path='/' render={ () => <Redirect to={'/candidats'} /> } />
          <Route path='/connexion' component={LoginForm} />
          <Route path='/candidats' component={Candidats} />
        </Switch>
      </BrowserRouter>
    );
  }
}
