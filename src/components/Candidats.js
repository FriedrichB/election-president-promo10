import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
import {Button} from 'antd';

import '../css/Candidats.css';



const styles = {
  slide: {
    padding: 15,
    minHeight: '100%',
    // color: '#fff',
    display: "flex",
    flexDirection:"column",
    flexWrap: "no-wrap",
    alignItems: "center"
  },
};

function Tabs(props) {
  return (
    <SwipeableViews enableMouseEvents>
        {props.data.candidates.map( (elt, index) => {
            return(
                <div style={Object.assign({}, styles.slide)} key={index}>
                    <img src={`${process.env.PUBLIC_URL}/${elt.pp}`} className="pp" />
                    <h1>{elt.nom}</h1>
                    <span>candidat n°{index+1}</span>
                    <Button onClick={() => props.voter(elt.id)}>VOTER</Button>
                </div>
            );
        })}
      
    </SwipeableViews>
  );
}



export default class Candidats extends Component{


    state={
        isLoading: true,
    }

    storeDef;

    componentWillMount(){
        fetch(`${process.env.PUBLIC_URL}/store.json`)
        .then(resp => resp.json())
        .then(resp => {
            this.storeDef = resp;
            console.log(this.storeDef);
            this.setState({isLoading: false});
        })
        .catch(err => console.log(err));
    }

    handleVote(id){
        //Incrementer le nombre de voix ici
    }

    render(){

        // if(!localStorage.electeurPromo10) return <Redirect to='/connexion' />;
        if(this.state.isLoading) return(
            <div className="store">
                Loading...
            </div>
        );

        return(
            <div className="store">
                <Tabs data={this.storeDef} voter={this.handleVote} />
            </div>
        );
    }
}