import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import { Form, Icon, Input, Button } from 'antd';


import '../css/Connexion.css';


class Connexion extends Component{

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              //Authetifier ici
          }
        });
      };

    render(){

        if(localStorage.electeurPromo10) return <Redirect to='/candidats' />;

        const { getFieldDecorator } = this.props.form;

        return(
            <div className="store">
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('code', {
                        rules: [{ required: true, message: 'Veuillez entrer votre code' }],
                    })(
                        <Input
                        prefix={<Icon type="qrcode" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Code"
                        />,
                    )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Voir les candidats
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

const LoginForm = Form.create({ name: 'normal_login' })(Connexion);

export default LoginForm;