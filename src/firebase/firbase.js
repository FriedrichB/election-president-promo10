import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyCvzmPiW4ftknojM-c0yx_Ee6ab2TC3Rro",
  authDomain: "election-promo10.firebaseapp.com",
  databaseURL: "https://election-promo10.firebaseio.com",
  projectId: "election-promo10",
  storageBucket: "",
  messagingSenderId: "322838593174",
  appId: "1:322838593174:web:e4503ec90ef0dd1b790cb7"
};

  firebase.initializeApp(firebaseConfig);

  export default firebase;